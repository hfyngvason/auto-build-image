variables:
  DOCKER_DRIVER: overlay2

  # explicit tag to be used within pipeline
  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"

  # we also publish a floating tag to simplify manual testing
  BUILD_IMAGE_NAME_LATEST: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:latest"

  DOCKER_VERSION: 19.03.5

  CI_APPLICATION_TAG: "$CI_COMMIT_SHA"
  CI_APPLICATION_REPOSITORY: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/$CI_JOB_NAME"

services:
  - "docker:${DOCKER_VERSION}-dind"

stages:
  - build
  - test
  - test-run
  - release

build:
  stage: build
  image: "docker:${DOCKER_VERSION}"
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build --build-arg DOCKER_VERSION="$DOCKER_VERSION"
      --tag "$BUILD_IMAGE_NAME"
      --tag "$BUILD_IMAGE_NAME_LATEST"
      .
    - docker push "$BUILD_IMAGE_NAME"
    - docker push "$BUILD_IMAGE_NAME_LATEST"

test-shellcheck:
  stage: test
  image: koalaman/shellcheck-alpine
  script:
    - shellcheck src/build.sh

test-dockerfile:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  script:
    - cd "$CI_PROJECT_DIR/test/ruby_with_dockerfile"
    - /build/build.sh | tee /tmp/build-output
    - grep 'Building Dockerfile-based application...' /tmp/build-output

# This test is to prevent a regression until https://github.com/docker/engine/pull/339 is out
# See https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/issues/13 for context
test-redhat-registry:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  script:
    - cd "$CI_PROJECT_DIR/test/redhat_registry"
    - /build/build.sh

test-herokuish:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  script:
    - cd "$CI_PROJECT_DIR/test/ruby"
    - /build/build.sh | tee /tmp/build-output
    - grep 'Building Heroku-based application using gliderlabs/herokuish docker image...' /tmp/build-output

test-run-herokuish:
  stage: test-run
  needs: ["test-herokuish"]
  services:
    - name: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/test-herokuish:$CI_APPLICATION_TAG"
      alias: app
  script:
    - wget app:5000
    - grep 'Hello World!' index.html

test-cnb:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  variables:
    AUTO_DEVOPS_BUILD_IMAGE_CNB_ENABLED: "true"
  script:
    - cd "$CI_PROJECT_DIR/test/ruby"
    - /build/build.sh | tee /tmp/build-output
    - grep 'Building Cloud Native Buildpack-based application with builder' /tmp/build-output

test-run-cnb:
  stage: test-run
  needs: ["test-cnb"]
  variables:
    AUTO_DEVOPS_BUILD_IMAGE_CNB_ENABLED: "true"
  services:
    - name: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/test-cnb:$CI_APPLICATION_TAG"
      alias: app
  script:
    - wget app:5000
    - grep 'Hello World!' index.html

test-herokuish-with-build-secrets:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  variables:
    FORWARDED_VARIABLE_0: 'my forwarded variable 0'
    FORWARDED_VARIABLE_1: 'my forwarded variable 1'
    AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES: FORWARDED_VARIABLE_0,FORWARDED_VARIABLE_1
  script:
    - cd "$CI_PROJECT_DIR/test/ruby"
    # When BuildKit is enabled, docker logs go to stderr by design https://github.com/moby/moby/issues/40031
    - /build/build.sh 2>&1 | tee /tmp/build-output
    - grep 'Building Heroku-based application using gliderlabs/herokuish docker image...' /tmp/build-output
    - grep 'FORWARDED_VARIABLE_0="my forwarded variable 0"' /tmp/build-output
    - grep 'FORWARDED_VARIABLE_1="my forwarded variable 1"' /tmp/build-output

test-cnb-with-build-secrets:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  variables:
    AUTO_DEVOPS_BUILD_IMAGE_CNB_ENABLED: "true"
    FORWARDED_VARIABLE_0: 'my forwarded variable 0'
    FORWARDED_VARIABLE_1: 'my forwarded variable 1'
    AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES: FORWARDED_VARIABLE_0,FORWARDED_VARIABLE_1
  script:
    - cd "$CI_PROJECT_DIR/test/ruby"
    # When BuildKit is enabled, docker logs go to stderr by design https://github.com/moby/moby/issues/40031
    - /build/build.sh 2>&1 | tee /tmp/build-output
    - grep 'Building Cloud Native Buildpack-based application with builder' /tmp/build-output
    - grep 'FORWARDED_VARIABLE_0="my forwarded variable 0"' /tmp/build-output
    - grep 'FORWARDED_VARIABLE_1="my forwarded variable 1"' /tmp/build-output

test-herokuish-with-buildpack-url:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  variables:
    BUILDPACK_URL: https://github.com/HashNuke/heroku-buildpack-elixir.git
  script:
    - cd "$CI_PROJECT_DIR/test/elixir"
    - /build/build.sh | tee /tmp/build-output
    - grep 'Building Heroku-based application using gliderlabs/herokuish docker image...' /tmp/build-output
    - grep "Using default config from Elixir buildpack" /tmp/build-output

test-cnb-with-buildpack-url:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  variables:
    AUTO_DEVOPS_BUILD_IMAGE_CNB_ENABLED: "true"
    BUILDPACK_URL: /tmp/elixir_buildpack
  before_script:
    # prepare CNB-compatible elixir buildpack
    - apk add curl
    - mkdir "$BUILDPACK_URL"
    - cd "$BUILDPACK_URL"
    - curl -L https://github.com/heroku/cnb-shim/releases/download/v0.2/cnb-shim-v0.2.tgz | tar xz
    - |
      cat > buildpack.toml << TOML
      api = "0.2"

      [buildpack]
      id = "hashnuke.elixir"
      version = "0.1"
      name = "Elixir"

      [[stacks]]
      id = "heroku-18"
      TOML
    - bin/install buildpack.toml https://buildpack-registry.s3.amazonaws.com/buildpacks/hashnuke/elixir.tgz
  script:
    - cd "$CI_PROJECT_DIR/test/elixir"
    - /build/build.sh | tee /tmp/build-output
    - grep 'Building Cloud Native Buildpack-based application with builder' /tmp/build-output
    - grep "Using default config from Elixir buildpack" /tmp/build-output

test-rspec:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  before_script:
    - gem install --no-document bundler
    - bundle install --without rubocop
  script:
    - bundle exec rspec

test-rubocop:
  stage: test
  image: ruby:2.6
  before_script:
    - gem install bundler
    - bundle install --without test
  script:
    - bundle exec rubocop Gemfile src spec

.semantic-release:
  image: node:12
  stage: release
  before_script:
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME
  only:
    variables:
      - $CI_API_V4_URL == "https://gitlab.com/api/v4"

release-tag:
  stage: release
  image: "docker:${DOCKER_VERSION}"
  services:
    - "docker:${DOCKER_VERSION}-dind"
  script:
    - 'echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY'
    - export ci_image="${CI_REGISTRY_IMAGE}"
    - export ci_image_tag=${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}
    - echo "Using tag $ci_image_tag for image"
    - docker pull "$BUILD_IMAGE_NAME"
    - docker tag "$BUILD_IMAGE_NAME" $ci_image:latest
    - docker tag "$BUILD_IMAGE_NAME" $ci_image:$ci_image_tag
    - docker push $ci_image:latest
    - docker push $ci_image:$ci_image_tag
  only:
    - tags

publish:
  extends: .semantic-release
  only:
    refs:
      - master@gitlab-org/cluster-integration/auto-build-image

publish-dryrun:
  extends: .semantic-release
  variables:
    DRY_RUN_OPT: '-d'
  only:
    - branches@gitlab-org/cluster-integration/auto-build-image
  except:
    refs:
      - master
